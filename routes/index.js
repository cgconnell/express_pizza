var express = require('express');
var router = express.Router();
var querystring = require('querystring');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');


/* GET home page. */
router.get('/', function(req, res, next) {
	res.cookie('message', 'pizza is good');
  res.render('index', { title: 'Pizza Menu', request: req, response: res });
});

router.post('/', function(req, res, next) {
	res.cookie('message', 'order received');
	res.render('pizza', { title: 'Pizza Order Submitted', request: req, response: res });
})

module.exports = router;